#!/bin/bash
# operate kingdee aas container

PID=$(ps aux | grep /home/www/AAS-V9.0/domains/sca/lib/ | grep -v grep | awk '{print $2}')
if [ -z "$PID" ]
then
	echo sca is already stopped
else
	echo killing $PID ...
	kill -9 $PID
	echo sca stopped successfully
fi

